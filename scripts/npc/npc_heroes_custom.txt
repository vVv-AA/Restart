"DOTAHeroes"
{
  "npc_dota_hero_abaddon_custom"
  {
    "AbilityLayout"     "4"
    "override_hero"     "npc_dota_hero_abaddon"	
		"Model"						"models/heroes/abaddon/abaddon.mdl"								// Model.
		"Portrait"					"vgui/hud/heroportraits/portrait_abaddon"						// Small image for hero selection
		"SoundSet"					"Hero_Abaddon"													// Name of sound set.
		"Role"						"Durable,Support,Carry"
		"Rolelevels"				"3,2,1"
		"ModelScale" 				"0.78"
		"LoadoutScale" 				"0.73"
		"NameAliases"				"Abaddon"
		"url" 						"Abaddon"
		"PickSound"					"abaddon_abad_spawn_01"
		"BanSound"					"abaddon_abad_anger_07"
		"HeroSelectSoundEffect"		"Hero_Abaddon.Pick"
		
	"Ability1"					"abaddon_soul_rip"
	"Ability2"					"abaddon_aphotic_shield_custom"
	"Ability3"					"ability_abaddon_curse_avernus"
	"Ability4"					"abaddon_possession"
	"Ability5"					"attribute_bonus"

	// Armor
	//-------------------------------------------------------------------------------------------------------------
	"ArmorPhysical"				"-1.0"								// Physical protection.
    "AttackCapabilities"    "DOTA_UNIT_CAP_MELEE_ATTACK"
	"AttackDamageMin"			"32"									// Damage range min.
	"AttackDamageMax"			"42"									// Damage range max.
	"AttackRate"				"1.7"									// Speed of attack.
	"AttackAnimationPoint"		"0.56"									// Normalized time in animation cycle to attack.
	"AttackAcquisitionRange"	"600"									// Range within a target can be acquired.
	"AttackRange"				"128"									// Range within a target can be attacked.

	// Attributes
	//-------------------------------------------------------------------------------------------------------------
	"AttributePrimary"				"DOTA_ATTRIBUTE_STRENGTH"
	"AttributeBaseStrength"			"23"									// Base strength
	"AttributeStrengthGain"			"2.7"									// Strength bonus per level.
	"AttributeBaseAgility"			"17"									// Base agility
	"AttributeAgilityGain"			"1.5"									// Agility bonus per level.
	"AttributeBaseIntelligence"		"21"									// Base intelligence
	"AttributeIntelligenceGain"		"2.0"									// Intelligence bonus per level.

		// Movement
		//-------------------------------------------------------------------------------------------------------------
		"MovementSpeed"				"310"									// Speed.
		"MovementTurnRate"			"0.5"									// Turning rate.
		"HasAggressiveStance"                  "1"                                         // Plays alternate idle/run animation when near enemies
		"HealthBarOffset"			"175"
		
		"ParticleFile"				"particles/units/heroes/hero_abaddon.pcf"
		"GameSoundsFile"			"scripts/game_sounds_heroes/game_sounds_abaddon.txt"
		"VoiceFile"				"scripts/voscripts/game_sounds_vo_abaddon.txt"

		// Additional data needed to render the out of game portrait
		"RenderablePortrait"
		{
			"Particles"
			{
				"abaddon_loadout"
				{
					"0"
					{
						"type"		"follow_origin"
						"location"	"attach_hitloc"
					}
				}	
			}
		}	
  }



  //=================================================================================================================
	// HERO: Omniknight
	//=================================================================================================================
	"npc_dota_hero_arthas"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		    "override_hero"     "npc_dota_hero_omniknight"	

		"Model"			"models/heroes/omniknight/omniknight.mdl"	// Model.
		"Portrait"		"vgui/hud/heroportraits/portrait_omniknight"		// Small image for hero selection
		"IdleExpression"	"scenes/omniknight/omniknight_exp_idle_01.vcd"		// custom facial animation idle
		"SoundSet"		"Hero_Omniknight"									// Name of sound set.
		"Enabled"		"1"
		"BotImplemented"			"1"
		"HeroPool1"					"1"
		"HeroUnlockOrder"		"3"
		"Role"			"Durable,LaneSupport,Support"
		"Rolelevels"			"2,1,1"
		"Team"			"Good"
		"ModelScale"					".74"
		"LoadoutScale"          ".81"
		"HeroGlowColor" 			"120 205 255"	   
		"CMEnabled"					"1"
		"PickSound"					"omniknight_omni_spawn_01"
		"BanSound"					"omniknight_omni_anger_04"
		"url"					"Arthas"
		"LastHitChallengeRival"		"npc_dota_hero_dragon_knight"
		"HeroSelectSoundEffect"		"Hero_Omniknight.Pick"


		// Abilities
		//-------------------------------------------------------------------------------------------------------------
		"Ability1"				"holy_shock"
		"Ability2"				"shield_of_the_righteous"
		"Ability3"				"sacred_duty_datadriven"
		"Ability4"				"cleanse"

		// Armor
		//-------------------------------------------------------------------------------------------------------------
		"ArmorPhysical"			"3"										// Physical protection.

		// Attack
		//-------------------------------------------------------------------------------------------------------------
		"AttackCapabilities"		"DOTA_UNIT_CAP_MELEE_ATTACK"
		"AttackDamageMin"			"31"									// Damage range min.
		"AttackDamageMax"			"41"									// Damage range max.
		"AttackRate"				"1.7"									// Speed of attack.
		"AttackAnimationPoint"		"0.433"									// Normalized time in animation cycle to attack.
		"AttackAcquisitionRange"	"600"									// Range within a target can be acquired.
		"AttackRange"				"128"									// Range within a target can be attacked.

		// Attributes
		//-------------------------------------------------------------------------------------------------------------
		"AttributePrimary"			"DOTA_ATTRIBUTE_STRENGTH"
		"AttributeBaseStrength"		"20"									// Base strength
		"AttributeStrengthGain"		"2.65"									// Strength bonus per level.
		"AttributeBaseAgility"		"15"									// Base agility
		"AttributeAgilityGain"		"1.75"									// Agility bonus per level.
		"AttributeBaseIntelligence"	"17"									// Base intelligence
		"AttributeIntelligenceGain"	"1.8"									// Intelligence bonus per level.

		// Movement
		//-------------------------------------------------------------------------------------------------------------
		"MovementSpeed"				"305"									// Speed.
		"MovementTurnRate"			"0.6"									// Turning rate.

		// Bounds
		//-------------------------------------------------------------------------------------------------------------
		"BoundsHullName"			"DOTA_HULL_SIZE_HERO"
		"HealthBarOffset"			"145"


		"VoiceFile"				"scripts/voscripts/game_sounds_vo_omniknight.txt"
		"ParticleFile"			"particles/units/heroes/hero_omniknight.pcf"
		"GameSoundsFile"		"scripts/game_sounds_heroes/game_sounds_omniknight.txt"

		// Additional data needed to render the out of game portrait
		
		"RenderablePortrait"
		{
			"Particles"
			{
				"omniknight_loadout"
				{
					"0"
					{
						"type"		"follow_attachment"
						"location"	"attach_hitloc"
					}
				}	
			}
		}				
	}

		//=================================================================================================================
	// HERO: Antimage
	//=================================================================================================================
	"npc_dota_hero_antimage_illidan"
	{																		
			    "override_hero"     "npc_dota_hero_antimage"	

		// General
		//-------------------------------------------------------------------------------------------------------------
		"Model"						"models/heroes/antimage/antimage.mdl"			// Model.
		"AbilityLayout"               "5"
		"SoundSet"					"Hero_Antimage"							// Name of sound set.
		"IdleExpression"			"scenes/antimage/antimage_exp_idle_01.vcd"		// custom facial animation idle
		"Enabled"					"1"
		"HeroUnlockOrder"			"1"
		"Role"						"Carry,Escape"
		"Rolelevels"				"2,3"
		"Team"						"Good"
		"Portrait"					"vgui/hud/heroportraits/portrait_antimage"
		"ModelScale" 				".90"
		"HeroGlowColor" 			"120 64 148"
		"PickSound"					"antimage_anti_spawn_01"
		"BanSound"					"antimage_anti_anger_04"
		"CMEnabled"					"1"
		"NameAliases"				"am"
		"url"				"Anti-Mage"
		"LastHitChallengeRival"		"npc_dota_hero_bounty_hunter"
		"HeroSelectSoundEffect"		"Hero_Antimage.Pick"

		// Abilities
		//-------------------------------------------------------------------------------------------------------------
		"Ability1"					"dive_datadriven"					// Ability 1
		"Ability2"					"sweeping_strike"						// Ability 2
		"Ability3"					"evasion"				// Ability 3
		"Ability4"					"hunters_thirst"					// Ability 4
		"Ability5"					"metamorphosis"					// Ability 5
		"Ability6"					"attribute_bonus"		//Ability 6
		// Armor
		//-------------------------------------------------------------------------------------------------------------
		"ArmorPhysical"				"-1"										// Physical protection.

		// Attack
		//-------------------------------------------------------------------------------------------------------------
		"AttackCapabilities"		"DOTA_UNIT_CAP_MELEE_ATTACK"
		"AttackDamageMin"			"27"									// Damage range min.
		"AttackDamageMax"			"31"									// Damage range max.	
		"AttackRate"				"1.45"									// Speed of attack.
		"AttackAnimationPoint"		"0.3"									// Normalized time in animation cycle to attack.
		"AttackAcquisitionRange"	"600"									// Range within a target can be acquired.
		"AttackRange"				"128"									// Range within a target can be attacked.
		"ProjectileModel"			""										// Particle system model for projectile.
		"ProjectileSpeed"			"0"										// Speed of projectile.

		// Attributes
		//-------------------------------------------------------------------------------------------------------------
		"AttributePrimary"			"DOTA_ATTRIBUTE_AGILITY"
		"AttributeBaseStrength"		"22"									// Base strength
		"AttributeStrengthGain"		"1.2"									// Strength bonus per level.
		"AttributeBaseIntelligence"	"15"									// Base intelligence
		"AttributeIntelligenceGain"	"1.8"									// Intelligence bonus per level.
		"AttributeBaseAgility"		"22"									// Base agility
		"AttributeAgilityGain"		"2.8"									// Agility bonus per level.

		// Movement
		//-------------------------------------------------------------------------------------------------------------
		"MovementSpeed"				"315"									// Speed.
		"MovementTurnRate"			"0.5"									// Turning rate.
		
		"BoundsHullName"			"DOTA_HULL_SIZE_HERO"
		"HealthBarOffset"			"140"

		"ParticleFile"				"particles/units/heroes/hero_antimage.pcf"
		"GameSoundsFile"			"scripts/game_sounds_heroes/game_sounds_antimage.txt"
		"VoiceFile" 				"scripts/voscripts/game_sounds_vo_antimage.txt"
		
		// Additional data needed to render the out of game portrait
		"RenderablePortrait"
		{
			"Particles"
			{
				"antimage_loadout"
				{
					"0"
					{
						"type"		"follow_origin"
						"location"	"attach_hitloc"
					}
				}	
			}
		}
	}

	"npc_dota_hero_necromancer"	
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"Model"						"models/heroes/necrolyte/necrolyte.mdl"		// Model.
		"override_hero"				"npc_dota_hero_necrolyte"
		"Portrait"					"vgui/hud/heroportraits/portrait_necrolyte"	// Small image for hero selection
		"IdleExpression"			"scenes/necrolyte/necrolyte_exp_idle_01.vcd"		// custom facial animation idle
		"SoundSet"					"Hero_Necrolyte"				// Name of sound set.
		"IdleSoundLoop"				"Hero_Necrolyte.IdleLoop"
		"Enabled"					"1"
		"BotImplemented"			"1"
		"HeroUnlockOrder"			"0"
		"Role"						"Carry,Durable"
		"Rolelevels"				"2,2"
		"Team"						"Bad"
		"ModelScale" 				".79"
		"HeroGlowColor" 			"146 255 145"
		"PickSound"					"necrolyte_necr_spawn_01"
		"BanSound"					"necrolyte_necr_anger_01"
		"CMEnabled"					"1"
		"url"						"Necrophos"
		"new_player_enable"			"1"
		"LastHitChallengeRival"		"npc_dota_hero_lich"
		"HeroSelectSoundEffect"		"Hero_Necrolyte.Pick"


		// Abilities
		//-------------------------------------------------------------------------------------------------------------
		"Ability1"					"raise_dead"					// Ability 1
		"Ability2"					"sacrifice"				// Ability 2
		"Ability3"					"call_of_the_dead"					// Ability 3
		"Ability4"					"monster_reborn_datadriven"				// Ability 4

		// Armor
		//-------------------------------------------------------------------------------------------------------------
		"ArmorPhysical"				"0"									// Physical protection.

		// Attack
		//-------------------------------------------------------------------------------------------------------------
		"AttackCapabilities"		"DOTA_UNIT_CAP_RANGED_ATTACK"
		"AttackDamageMin"			"22"									// Damage range min.
		"AttackDamageMax"			"26"									// Damage range max.
		"AttackRate"				"1.7"									// Speed of attack.
		"AttackAnimationPoint"			"0.53"									// Normalized time in animation cycle to attack.
		"AttackAcquisitionRange"		"800"									// Range within a target can be acquired.
		"AttackRange"				"550"									// Range within a target can be attacked.
		"ProjectileModel"			"necrolyte_base_attack"							// Particle system model for projectile.
		"ProjectileSpeed"			"900"									// Speed of projectile.

		// Attributes
		//-------------------------------------------------------------------------------------------------------------
		"AttributePrimary"				"DOTA_ATTRIBUTE_INTELLECT"
		"AttributeBaseStrength"			"16"									// Base strength
		"AttributeStrengthGain"			"2.0"									// Strength bonus per level.
		"AttributeBaseIntelligence"		"22"									// Base intelligence
		"AttributeIntelligenceGain"		"2.5"									// Intelligence bonus per level.
		"AttributeBaseAgility"			"15"									// Base agility
		"AttributeAgilityGain"			"1.7"									// Agility bonus per level.

		// Movement
		//-------------------------------------------------------------------------------------------------------------
		"MovementSpeed"				"290"									// Speed.
		"MovementTurnRate"			"0.5"									// Turning rate.
		
		// Bounds
		//-------------------------------------------------------------------------------------------------------------
		"BoundsHullName"			"DOTA_HULL_SIZE_HERO"
		"HealthBarOffset"			"160"

		"ParticleFile"				"particles/units/heroes/hero_necrolyte.pcf"
		"GameSoundsFile"			"scripts/game_sounds_heroes/game_sounds_necrolyte.txt"
		"VoiceFile"				"scripts/voscripts/game_sounds_vo_necrolyte.txt"

		// Additional data needed to render the out of game portrait
		"RenderablePortrait"
		{
			"Particles"
			{
				"necrolyte_loadout"
				{
					"0"
					{
						"type"		"follow_origin"
						"location"		"attach_hitloc"
					}
				}
			}
		}
	}
	

		//=================================================================================================================
	// HERO: Windrunner
	//=================================================================================================================
	"npc_dota_hero_artemis"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"override_hero"				"npc_dota_hero_windrunner"
		"Model"					"models/heroes/windrunner/windrunner.mdl"				// Model.
		"Portrait"				"vgui/hud/heroportraits/portrait_windrunner"		// Small image for hero selection
		"IdleExpression"		"scenes/windrunner/windrunner_exp_idle_01.vcd"		// custom facial animation idle
		"SoundSet"				"Hero_Windrunner"							// Name of sound set.
		"PickSound"					"windrunner_wind_spawn_01"
		"BanSound"					"windrunner_wind_anger_01"
		"Enabled"				"1"
		"HeroPool1"					"1"
		"HeroUnlockOrder"		"2"
		"Role"					"Disabler,Nuker,Support,Escape"
		"Rolelevels"		"1,1,1,1"
		"Team"					"Good"	
		"BotImplemented"			"1"
		"ModelScale"					"0.98"
		"LoadoutScale"			"1.1"
		"HeroGlowColor" 			"185 220 20"
		"CMEnabled"					"1"
		"NameAliases"			"wr"
		"url"			"Windranger"
		"LastHitChallengeRival"		"npc_dota_hero_drow_ranger"
		"HeroSelectSoundEffect"		"Hero_Windrunner.Pick"

		// Abilities
		//-------------------------------------------------------------------------------------------------------------
		"Ability1"				"aimed_shot"					// Ability 1
		"Ability2"				"shadow_in_the_shadows"						// Ability 2
		"Ability3"				"track"						// Ability 3
		"Ability4"				"brew_selector"						// Ability 4

		// Armor
		//-------------------------------------------------------------------------------------------------------------
		"ArmorPhysical"				"-1"									// Physical protection.

		// Attack
		//-------------------------------------------------------------------------------------------------------------
		"AttackCapabilities"		"DOTA_UNIT_CAP_RANGED_ATTACK"
		"AttackDamageMin"			"22"									// Damage range min.
		"AttackDamageMax"			"34"									// Damage range max.
		"AttackRate"				"1.5"									// Speed of attack.
		"AttackAnimationPoint"		"0.4"									// Normalized time in animation cycle to attack.
		"AttackAcquisitionRange"	"800"									// Range within a target can be acquired.
		"AttackRange"				"600"									// Range within a target can be attacked.
		"ProjectileModel"			"windrunner_base_attack"				// Particle system model for projectile.
		"ProjectileSpeed"			"1250"									// Speed of projectile.

		// Attributes
		//-------------------------------------------------------------------------------------------------------------
		"AttributePrimary"				"DOTA_ATTRIBUTE_INTELLECT"
		"AttributeBaseStrength"			"15"									// Base strength
		"AttributeStrengthGain"			"2.5"									// Strength bonus per level.
		"AttributeBaseIntelligence"		"22"									// Base intelligence
		"AttributeIntelligenceGain"		"2.6"									// Intelligence bonus per level.
		"AttributeBaseAgility"			"17"									// Base agility
		"AttributeAgilityGain"			"1.4"									// Agility bonus per level.
		
		// Bounds
		//-------------------------------------------------------------------------------------------------------------
		"BoundsHullName"			"DOTA_HULL_SIZE_HERO"
		"HealthBarOffset"			"160"

		// Movement
		//-------------------------------------------------------------------------------------------------------------
		"MovementSpeed"				"295"									// Speed.
		"MovementTurnRate"			"0.6"									// Turning rate.
		
		"ParticleFile"				"particles/units/heroes/hero_windrunner.pcf"
		"GameSoundsFile"			"scripts/game_sounds_heroes/game_sounds_windrunner.txt"
		"VoiceFile"				"scripts/voscripts/game_sounds_vo_windrunner.txt"

		// Additional data needed to render the out of game portrait
		
		"RenderablePortrait"
		{
			"Particles"
			{
				"windrunner_loadout"
				{
					"0"
					{
						"type"		"follow_origin"
						"location"	"attach_hitloc"
					}
				}	
			}
		}
	}

"npc_dota_hero_brew_master"	
	{
		"override_hero"				"npc_dota_hero_alchemist"
		// General
		//-------------------------------------------------------------------------------------------------------------
		"Model"						"models/heroes/alchemist/alchemist.mdl"		// Model.
		"Portrait"					"vgui/hud/heroportraits/portrait_alchemist"						// Small image for hero selection
		"IdleExpression"				"scenes/alchemist/alchemist_exp_idle_01.vcd"		// custom facial animation idle
		"SoundSet"					"Hero_Alchemist"													// Name of sound set.
		"Enabled"					"1"
		"AbilityLayout"               "6"
		"HeroUnlockOrder"			"1"
		"Role"						"Durable,Carry,Disabler"
		"Rolelevels"			"2,1,1"
		"Team"						"Good"
		"ModelScale" 					"0.93"
		"LoadoutScale" 				"0.82"
		"CMEnabled"					"1"
		"PickSound"					"alchemist_alch_spawn_01"
		"BanSound"					"alchemist_alch_anger_05"
		"url"					"Alchemist"
		"LastHitChallengeRival"		"npc_dota_hero_earthshaker"
		"HeroSelectSoundEffect"		"Hero_Alchemist.Pick"

		// Abilities
		//-------------------------------------------------------------------------------------------------------------
		"Ability1"					"brew_selector"
		"Ability2"					"brew_lua_based"
		"Ability3"					"jump_spell_datadriven"
		"Ability4"					"aimed_shot"
		"Ability5"					"abaddon_soul_rip"
		"Ability6"					"abaddon_aphotic_shield_custom"
		"Ability7"					"brew_end_datadriven"
		"Ability8"					"attribute_bonus"

		// Armor
		//-------------------------------------------------------------------------------------------------------------
		"ArmorPhysical"				"0"								// Physical protection.
									//-1.03143

		// Attack
		//-------------------------------------------------------------------------------------------------------------
		"AttackCapabilities"		"DOTA_UNIT_CAP_MELEE_ATTACK"
		"AttackDamageMin"			"24"									// Damage range min.
		"AttackDamageMax"			"33"									// Damage range max.
		"AttackRate"				"1.7"									// Speed of attack.
		"AttackAnimationPoint"		"0.35"									// Normalized time in animation cycle to attack.
		"AttackAcquisitionRange"	"600"									// Range within a target can be acquired.
		"AttackRange"				"128"									// Range within a target can be attacked.

		// Attributes
		//-------------------------------------------------------------------------------------------------------------
		"AttributePrimary"				"DOTA_ATTRIBUTE_STRENGTH"
		"AttributeBaseStrength"			"25"									// Base strength
		"AttributeStrengthGain"			"1.8"									// Strength bonus per level.
		"AttributeBaseAgility"			"11"									// Base agility
		"AttributeAgilityGain"			"1.2"									// Agility bonus per level.
		"AttributeBaseIntelligence"		"25"									// Base intelligence
		"AttributeIntelligenceGain"		"1.8"									// Intelligence bonus per level.

		// Movement
		//-------------------------------------------------------------------------------------------------------------
		"MovementSpeed"				"295"									// Speed.
		"MovementTurnRate"			"0.6"									// Turning rate.
		"HasAggressiveStance"		"1"										// Plays alternate idle/run animation when near enemies
		
		"HealthBarOffset"			"200"
		
		"ParticleFile"				"particles/units/heroes/hero_alchemist.pcf"
		"GameSoundsFile"			"scripts/game_sounds_heroes/game_sounds_alchemist.txt"
		"VoiceFile"					"scripts/voscripts/game_sounds_vo_alchemist.txt"
		
		// Additional data needed to render the out of game portrait
		
		"RenderablePortrait"
		{
			"Particles"
			{
				"alchemist_loadout"
				{
					"0"
					{
						"type"		"follow_attachment"
						"location"	"attach_hitloc"
					}
				}	
			}
		}
	}





	
}
