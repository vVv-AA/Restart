meta_bonus = class({})

function meta_bonus:DeclareFunctions()
local funcs = {
        MODIFIER_PROPERTY_HEALTH_BONUS,
        MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
}
return funcs
end
---
function meta_bonus:OnCreated( kv )  
    if IsServer() then
   	local ability = self:GetAbility()
    local caster = self:GetParent()
    local health_bonus = ability:GetLevelSpecialValueFor("health_bonus", (ability:GetLevel() - 1))
    local bonus_atk_speed = 100
    print(bonus_atk_speed)
    	self.health_bonus = health_bonus
        self.bonus_atk_speed = bonus_atk_speed
    end
end

function meta_bonus:IsPurgable()
return false
end

function meta_bonus:GetModifierExtraHealthBonus( params )
return self.health_bonus
end

function meta_bonus:GetModifierAttackSpeedBonus_Constant( params )
return self.bonus_atk_speed
end

function meta_bonus:GetModifierHealthBonus( params )
return self.health_bonus
end