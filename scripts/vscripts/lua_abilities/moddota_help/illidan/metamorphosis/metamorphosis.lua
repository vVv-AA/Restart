LinkLuaModifier( "meta_bonus", "lua_abilities/moddota_help/illidan/metamorphosis/meta_bonus.lua", LUA_MODIFIER_MOTION_NONE )

function MetamorphosisCast ( event )
		print("here1")

	local hero = event.caster
	local point = event.target_points[1]
	local ability = event.ability
	hero:AddNoDraw()
    hero.ShieldParticle = ParticleManager:CreateParticle("particles/units/heroes/hero_enigma/enigma_blackhole_n.vpcf", PATTACH_CUSTOMORIGIN, hero)
    ParticleManager:SetParticleControl(hero.ShieldParticle, 0, point)
    ParticleManager:SetParticleControl(hero.ShieldParticle, 1, point)
    hero:SetAbsOrigin(point)

	Timers:CreateTimer(0.01, function()
			if hero:HasModifier("modifier_coccon") ~= true then
				hero:RemoveNoDraw()
				ParticleManager:DestroyParticle(hero.ShieldParticle,false)
				return nil
			end
		return 0.03
	end)
end

function MetamorphosisStart ( event )
	if event.caster:HasModifier("meta_bonus") == true then ParticleManager:DestroyParticle(event.caster.meta_particle, false)
		end

	event.caster:AddNewModifier(event.caster, event.ability, "meta_bonus", {duration = event.ability:GetSpecialValueFor("duration")})
	event.caster:SetHealth(event.caster:GetHealth())
	event.caster.meta_particle = ParticleManager:CreateParticle("particles/dark_smoke_test.vpcf", PATTACH_ABSORIGIN_FOLLOW, event.caster)
	ParticleManager:SetParticleControl(event.caster.meta_particle, 0, event.caster:GetAbsOrigin())
		Timers:CreateTimer(0.01, function()
			if event.caster:HasModifier("meta_bonus") ~= true then
				ParticleManager:DestroyParticle(event.caster.meta_particle,false)
				return nil
			end
		return 0.03
	end)
end

function UpHuntersThirst( keys )
	local huntersthirst = keys.caster:FindAbilityByName("hunters_thirst")
	local metamorphosis_level = keys.ability:GetLevel()
	
	if metamorphosis_level ~= nil and metamorphosis_level + 1 ~= huntersthirst:GetLevel() then
		huntersthirst:SetLevel(metamorphosis_level + 1)
	end
end