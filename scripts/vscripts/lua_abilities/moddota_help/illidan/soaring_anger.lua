function SoaringAnger ( event )
  local hero = event.caster
  local target = event.target
  local ability = event.ability
  local duration = ability:GetLevelSpecialValueFor( "duration", ability:GetLevel() - 1 )
  local height = ability:GetLevelSpecialValueFor( "height", ability:GetLevel() - 1 )
  local damage = ability:GetLevelSpecialValueFor( "damage", ability:GetLevel() - 1 )
  local berserker_bonus = ability:GetLevelSpecialValueFor( "berserker_bonus", ability:GetLevel() - 1 )
  local radius = ability:GetLevelSpecialValueFor( "radius", ability:GetLevel() - 1 )
  local offset = ability:GetLevelSpecialValueFor( "offset", ability:GetLevel() - 1 )

  StartAnimation(hero, {duration=duration, activity=ACT_DOTA_ATTACK_EVENT, rate=0.25})

  Physics:Unit(hero)
  hero:PreventDI()
  local direction = hero:GetAbsOrigin() - target:GetAbsOrigin()
  direction = direction:Normalized()

  ProjectileManager:ProjectileDodge(hero)
  hero:SetPhysicsFriction(0)
  hero:SetPhysicsVelocityMax(1000)
  hero:SetPhysicsAcceleration(direction*9999999)

hero:OnPhysicsFrame(function(unit)
  -- Retarget acceleration vector
  local distance = hero:GetAbsOrigin() - target:GetAbsOrigin()
  local direction = -1*distance:Normalized()
  hero:SetPhysicsAcceleration(direction*9999999)
  hero:SetForwardVector(direction)

  -- Stop if reached the unit
    if distance:Length() < 125 then
    hero:SetPhysicsAcceleration(Vector(0,0,0))
    hero:SetPhysicsVelocity(Vector(0,0,0))
    if hero:HasModifier("modifier_soaring_anger") then
    hero:RemoveModifierByNameAndCaster("modifier_soaring_anger", hero)
    end
    
    hero:Stop() 
    
    local order =
    {
      UnitIndex = hero:entindex(),
      OrderType = DOTA_UNIT_ORDER_ATTACK_TARGET,
      TargetIndex = target:entindex(),
      Queue = true
    }

    ExecuteOrderFromTable(order)
    hero:OnPhysicsFrame(nil)
  end
end)
end


function Blink (keys)
  local caster = keys.caster
  local target = keys.target
  local point = target:GetAbsOrigin()
  local casterPos = caster:GetAbsOrigin()
  local pid = caster:GetPlayerID()
  local difference = point - casterPos
  local ability = keys.ability
  local range = ability:GetLevelSpecialValueFor("blink_range", (ability:GetLevel() - 1))
  point = point + (point - casterPos):Normalized() * 50
  local direction = (point - casterPos):Normalized()
  
  caster:Stop() 
  caster:SetForwardVector(direction*-1)
  FindClearSpaceForUnit(caster, point, false)
  ProjectileManager:ProjectileDodge(caster)
end