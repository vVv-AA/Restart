function SelfCheck( keys )
	if keys.caster == keys.target then
		keys.caster:Stop()
	end
end

function SetupEntities( keys )
	local target = keys.target
	local ability = keys.ability

	ability.charge_speed = ability:GetLevelSpecialValueFor("speed", (ability:GetLevel() - 1)) * 1/30
	ability.charge_target = target
end

function Dive( keys )
	local caster = keys.caster
	local ability = keys.ability
	local target = ability.charge_target

	local caster_location = caster:GetAbsOrigin() 
	local target_location = target:GetAbsOrigin()
	local distance = (target_location - caster_location)
	local direction = (distance):Normalized()
	local distance_length = distance:Length2D()

	if distance_length > ability.charge_speed then
		caster:SetAbsOrigin(caster_location + direction * ability.charge_speed)
	else
		caster:InterruptMotionControllers(false)
		caster:SetAbsOrigin(caster_location + direction * ability.charge_speed * 3)
		caster:RemoveModifierByNameAndCaster("diving", caster) 
		
		if target:GetTeam() ~= caster:GetTeam() then
			local damageTable = {
			victim = target,
			attacker = caster,
			damage = ability:GetAbilityDamage(),
			damage_type = DAMAGE_TYPE_PURE,
			}
			ApplyDamage(damageTable)

		    local order = {
		        UnitIndex = caster:entindex(),
		        TargetIndex = target:entindex(),
		        OrderType = DOTA_UNIT_ORDER_ATTACK_TARGET,
		    }
		    ExecuteOrderFromTable(order)
		end
	end
end

-- Credits: Pizzalol