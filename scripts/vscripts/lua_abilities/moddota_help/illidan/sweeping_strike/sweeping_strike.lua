function Sweeping( keys )
	local caster = keys.caster
	local caster_location = caster:GetAbsOrigin()
	local target_point = keys.target_points[1]
	local ability = keys.ability

	-- Distance calculations
	local speed = ability:GetLevelSpecialValueFor("speed", (ability:GetLevel() - 1))
	local distance = (target_point - caster_location):Length2D()
	local direction = (target_point - caster_location):Normalized()
	local duration = distance/speed

	-- Saving the data in the ability
	ability.time_walk_distance = distance
	ability.time_walk_speed = speed * 1/30 -- 1/30 is how often the motion controller ticks
	ability.time_walk_direction = direction
	ability.time_walk_traveled_distance = 0
	ability.targetsHit = {}
	ability.target = target_point
	-- Apply the slow aura and invlunerability modifier to the caster
	ability:ApplyDataDrivenModifier(caster, caster, "charging", {})
end

--[[Author: Pizzalol
	Date: 21.09.2015.
	Moves the target until it has traveled the distance to the chosen point]]
function SweepingMotion( keys )
	local caster = keys.target
	local ability = keys.ability

	-- Move the caster while the distance traveled is less than the original distance upon cast
	if ability.time_walk_traveled_distance < ability.time_walk_distance then
		caster:SetAbsOrigin(caster:GetAbsOrigin() + ability.time_walk_direction * ability.time_walk_speed)
		ability.time_walk_traveled_distance = ability.time_walk_traveled_distance + ability.time_walk_speed
		Damage(caster, ability)
	else
		-- Remove the motion controller once the distance has been traveled
		caster:InterruptMotionControllers(true)
		caster:RemoveModifierByNameAndCaster("charging", caster)
		ability:ApplyDataDrivenModifier(caster, caster, "bonus_damage", {duration = duration})
	end
end

function Damage( caster, ability )
	local units = FindUnitsInRadius(caster:GetTeamNumber(), caster:GetAbsOrigin() , nil, 125, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)

	for _,unit in pairs(units) do
		if unit ~= nil and ( not unit:IsMagicImmune() ) and ( not unit:IsInvulnerable() and unit ~= caster) and ability.targetsHit[unit] ~=true then
			local damagetable = {
				victim = unit,
				attacker = caster,
				damage = ability:GetAbilityDamage(),
				damage_type = ability:GetAbilityDamageType(),
				ability = ability
			}
			ApplyDamage( damagetable )
			ability.targetsHit[unit] = true
		end
	end
	-- body
end