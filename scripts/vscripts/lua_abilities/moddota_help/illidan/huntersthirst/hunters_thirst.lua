function CooldownReducton ( event )
	local caster = event.caster
	local ability = event.ability

	local ability1 = caster:GetAbilityByIndex(0)
	local ability2 = caster:GetAbilityByIndex(1)
	local ability3 = caster:GetAbilityByIndex(2)
	local ability4 = caster:GetAbilityByIndex(3) 
	-- Cooldowns
	local reduction = 1
	if caster:HasModifier("double_reduction") then reduction = 2 end
	
	if ability1:IsCooldownReady() ~= true then
		local cooldown1 = ability1:GetCooldownTimeRemaining()
		ability1:EndCooldown()
		ability1:StartCooldown(math.max(cooldown1-reduction, 0))
	end
	
	if ability2:IsCooldownReady() ~= true then
		local cooldown2 = ability2:GetCooldownTimeRemaining()
		ability2:EndCooldown()
		ability2:StartCooldown(math.max(cooldown2-reduction, 0))
	end
		
	if ability3:IsCooldownReady() ~= true then
		local cooldown3 = ability3:GetCooldownTimeRemaining()
		ability3:EndCooldown()
		ability3:StartCooldown(math.max(cooldown3-reduction, 0))
	end

	if ability4:IsCooldownReady() ~= true then
		local cooldown4 = ability4:GetCooldownTimeRemaining()
		ability4:EndCooldown()
		ability4:StartCooldown(math.max(cooldown4-reduction, 0))
	end

	-- Current ability handles, always on the first 2 slots
end