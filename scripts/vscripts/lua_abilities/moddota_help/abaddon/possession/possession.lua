function PossessionStart( keys )
	-- Particle stuff..todo
	--    local particle_name = "particles/units/heroes/hero_abaddon/abaddon_borrowed_time.vpcf"

	 --           local info = {
    --            Target = keys.target,
    --            Source = keys.caster,
    --            Ability = ability,
    --            EffectName = particle_name,
    --            bDodgeable = false,
    --            bProvidesVision = true,
    --            iMoveSpeed = 1200,
    --            iVisionRadius = 0,
    --            iVisionTeamNumber = keys.caster:GetTeamNumber(),
    --            iSourceAttachment = DOTA_PROJECTILE_ATTACHMENT_ATTACK_1
    --            }
    --            ProjectileManager:CreateTrackingProjectile( info )
--

end

function PossessionTakeCharge( keys )
	local caster = keys.caster
	local target = keys.target
	local ability = keys.ability
    local caster_team = caster:GetTeamNumber()
	local player = caster:GetPlayerID()
	local enemy = target:GetPlayerID()
	local target_team = target:GetTeamNumber()
	local time_elapsed = 0
	local str_bonus = caster:GetBaseStrength()
	local agi_bonus = caster:GetBaseAgility()
	local int_bonus = caster:GetBaseIntellect()
    local respawnPosition = caster:GetAbsOrigin()
    -- Projectile info
	local particle_name = "particles/units/heroes/hero_vengeful/vengeful_magic_missle.vpcf"
			    local info = {
			    Target = caster,
				Caster = target,
                Ability = null,
                EffectName = particle_name,
                bDodgeable = false,
                bProvidesVision = true,
                iMoveSpeed = 1200,
                iVisionRadius = 0,
                iVisionTeamNumber = caster:GetTeamNumber(),
                iSourceAttachment = DOTA_PROJECTILE_ATTACHMENT_ATTACK_1
                }

    target:Stop()
	-- Change the ownership of the Hero
	target:SetTeam(caster_team)
	target:SetOwner(caster)
	target:SetControllableByPlayer(player, true)
	target:ModifyStrength(str_bonus)
	target:ModifyAgility(agi_bonus)
	target:ModifyIntellect(int_bonus)

	Timers:CreateTimer(0, function()
		time_elapsed = time_elapsed+0.03
		if time_elapsed >= 6 or target:IsAlive() == false then
		target:ModifyStrength(-1*str_bonus)
		target:ModifyAgility(-1*agi_bonus)
		target:ModifyIntellect(-1*int_bonus)

		target:SetTeam(target_team)
		target:SetOwner(target)
		target:SetControllableByPlayer(enemy, true)
		if target:IsAlive() == false then
            caster:Kill(ability, caster)
            caster:SetRespawnPosition(respawnPosition)
        end
        --Release the projectile
		TrackingProjectile( info )
		--Remove modifier when projectile hits
		return nil
    end
        return 0.03
    end)

    ability.str_bonus = str_bonus
    ability.agi_bonus = agi_bonus
    ability.int_bonus = int_bonus
    ability.player = player
    ability.enemy = enemy
end

function TrackingProjectile( params )
    local target = params.Target
    local caster = params.Caster
    local speed = params.iMoveSpeed
 
    --Fetch initial projectile location
    local projectile = caster:GetAttachmentOrigin( params.iSourceAttachment )
 
    --Make the particle
    local particle = ParticleManager:CreateParticle( params.EffectName, PATTACH_CUSTOMORIGIN, caster)
    --Source CP
    ParticleManager:SetParticleControl( particle, 0, caster:GetAttachmentOrigin( params.iSourceAttachment ) )
    --TargetCP
    ParticleManager:SetParticleControlEnt( particle, 1, target, PATTACH_POINT_FOLLOW, "attach_hitloc", target:GetAbsOrigin(), true )
    --Speed CP
    ParticleManager:SetParticleControl( particle, 2, Vector( speed, 0, 0 ) )
 
    Timers:CreateTimer(function()
        --Get the target location
        local target_location = target:GetAbsOrigin()
 
        --Move the projectile towards the target
        projectile = projectile + ( target_location - projectile ):Normalized() * speed * FrameTime()
 
        --Check the distance to the target
        if ( target_location - projectile ):Length2D() < speed * FrameTime() then
            --Target has reached destination!
 			target:RemoveModifierByNameAndCaster("post_poss_stun", target) --[[Returns:void
 			Removes a modifier that was cast by the given caster
 			]]
            --Destroy particle
            ParticleManager:DestroyParticle( particle, false )
 
            --Stop the timer
            return nil
        else
            --Reschedule for next frame
            return 0
        end
    end)
end


--Credits: Perry

--TP problem will be resolved in TP item, it will disabled for the duration of the spell