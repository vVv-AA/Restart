if modifer_abaddon_soul_rip_lift == nil then
    modifer_abaddon_soul_rip_lift = class({})
end

function modifer_abaddon_soul_rip_lift:OnCreated( kv )
    local time_elapsed = 0
    --Checks if lua is run from server.dll then applies Particles and Lift Effect.
    if IsServer() then
        local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_rubick/rubick_telekinesis.vpcf", PATTACH_CUSTOMORIGIN, nil );
        ParticleManager:SetParticleControlEnt( nFXIndex, 0, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetOrigin(), true );
        ParticleManager:SetParticleControlEnt( nFXIndex, 1, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetCaster():GetOrigin(), true );
        ParticleManager:SetParticleControl(nFXIndex, 2, Vector(kv.duration,0,0))
        self:AddParticle(nFXIndex, false, false, 1, false, false) 

    --Create a zero delay timer that respects pauses
        Timers:CreateTimer(function()
        	            time_elapsed = time_elapsed + 0.03
            if time_elapsed < 0.18 then 
                self:GetParent():SetAbsOrigin(self:GetParent():GetAbsOrigin() + Vector(0, 0, 32))
            end
            if time_elapsed > kv.duration - 0.2 then
                    --Reset the unit to ground
                    self:GetParent():SetAbsOrigin(self:GetParent():GetAbsOrigin() - Vector(0, 0, 160))
            	return nil --Stop Timer
            end
            return 0.03
        end)
    end
end

function modifer_abaddon_soul_rip_lift:OnDestroy()
                    --Remove the lift effect
 end

function modifer_abaddon_soul_rip_lift:IsHidden()
    return false
end

function modifer_abaddon_soul_rip_lift:IsPurgable()
    return true
end

--List of modifiers
function modifer_abaddon_soul_rip_lift:DeclareFunctions()
    local funcs = {
    MODIFIER_PROPERTY_OVERRIDE_ANIMATION
    }

    return funcs
end

--Stun State check
function modifer_abaddon_soul_rip_lift:CheckState()
    local state = {
    [MODIFIER_STATE_STUNNED] = true
    }

    return state
end

--OverrideAnimation and make the target FLAIL
function modifer_abaddon_soul_rip_lift:GetOverrideAnimation( params )
    return ACT_DOTA_FLAIL
end

--Credits: DrTeaSpoon