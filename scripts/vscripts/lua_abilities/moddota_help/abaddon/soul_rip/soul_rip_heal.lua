if modifer_abaddon_soul_rip_heal == nil then
    modifer_abaddon_soul_rip_heal = class({})
end

function modifer_abaddon_soul_rip_heal:OnCreated( kv )  
    if IsServer() then
        self.heal = kv.heal
    end
end

function modifer_abaddon_soul_rip_heal:IsHidden()
    return false
end

function modifer_abaddon_soul_rip_heal:IsPurgable()
    return false
end

function modifer_abaddon_soul_rip_heal:GetAttributes() 
    return MODIFIER_ATTRIBUTE_MULTIPLE + MODIFIER_ATTRIBUTE_IGNORE_INVULNERABLE 
end

function modifer_abaddon_soul_rip_heal:DeclareFunctions( params )
	local funcs = {
	MODIFIER_PROPERTY_HEALTH_BONUS
	}
	return funcs
end

function modifer_abaddon_soul_rip_heal:GetModifierHealthBonus( params )
	return self.heal
end

--Credits: DrTeaSpoon