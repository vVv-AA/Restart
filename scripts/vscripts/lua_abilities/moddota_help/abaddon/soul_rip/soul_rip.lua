abaddon_soul_rip = class ({})
--Link Modifiers so they can be called here.
LinkLuaModifier( "modifer_abaddon_soul_rip_lift", "lua_abilities/moddota_help/abaddon/soul_rip/soul_rip_lift.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifer_abaddon_soul_rip_heal", "lua_abilities/moddota_help/abaddon/soul_rip/soul_rip_heal.lua", LUA_MODIFIER_MOTION_NONE )

--Executed OnSpellStart, after cast phase.
function abaddon_soul_rip:OnSpellStart()
        --Get entities
        local hTarget = self:GetCursorTarget()
        local  hCaster = self:GetCaster()
        --Check for no target or this ability triggers a spell absorb on the target.
        --Stop running the script if either condition is satisfied
        if hCaster == nil or hTarget == nil or hTarget:TriggerSpellAbsorb( this ) then
                return
        end
        --Interrupt target
        hTarget:Interrupt()

        --Get spell attributes
        local fDuration = self:GetSpecialValueFor( "duration" )
        local fHealDuration = self:GetSpecialValueFor( "heal_duration" )

        --[[
        local nPctDamage = self:GetSpecialValueFor("damage_pct") / 100
        local nDamage = hTarget:GetMaxHealth() * nPctDamage
        refer line 33, 34
        ]]

        --FireSoundEffect
        EmitSoundOnLocationWithCaster(hTarget:GetAbsOrigin(), "Hero_Abaddon.DeathCoil.Cast", hCaster) 

        --Apply Lift Modifer to the target
        hTarget:AddNewModifier(hCaster, self, "modifer_abaddon_soul_rip_lift", {duration = fDuration})

        --Calculate damage
        local tDamage = {
        victim = hTarget,
        attacker = self:GetCaster(),
        damage = hTarget:GetMaxHealth() * self:GetSpecialValueFor("damage_pct") / 100,
        damage_type = self:GetAbilityDamageType(),
        ability = self
        }

        --ApplyDamage to the target and store it
        local nHeal = ApplyDamage(tDamage)
        --Heals the caster
        hCaster:Heal(nHeal, hCaster)
        --Apply the Bonus Health Modifier on caster
        hCaster:AddNewModifier(hCaster, self, "modifer_abaddon_soul_rip_heal", {duration = fHealDuration, heal = nHeal})
end

--[[
Lines repositioned to:
i.  Move Interrupt function up to prevent clutch escapes and apply interrupt ASAP
ii. Lift follows it to apply stun ASAP to prevent clutch moments.
]]