modifier_soul_collector=class({})


function modifier_soul_collector:IsPurgable()
return false
end
 
function modifier_soul_collector:IsBuff()
return true
end
 
function modifier_soul_collector:DeclareFunctions()
return {
        MODIFIER_EVENT_ON_ATTACK_LANDED,
        MODIFIER_EVENT_ON_DEATH,
        MODIFIER_PROPERTY_PREATTACK_BONUS_DAMAGE,
        MODIFIER_EVENT_ON_HERO_KILLED,
}
end
----
function modifier_soul_collector:OnAttackLanded(params)
if not IsServer() then return end
if params.attacker==self:GetCaster() then
params.target:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_frostmourne", {duration=self:GetAbility():GetSpecialValueFor("debuff_duration")})
self:GetCaster():AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_frostmourne_buff",{duration=self:GetAbility():GetSpecialValueFor("buff_duration")})
end
end
----
function modifier_soul_collector:GetModifierPreAttack_BonusDamage()
return self:GetStackCount()*self:GetAbility():GetSpecialValueFor("necromastery_damage_per_soul")
end
----
function modifier_soul_collector:IncStackValue(value)
self:SetStackCount(math.max(0,self:GetStackCount()+(value)))
end
----
function modifier_soul_collector:OnHeroKilled(params)
	if not IsServer() then return end
if params.attacker==self:GetCaster() then
	self:IncStackValue(self:GetAbility():GetSpecialValueFor("necromastery_souls_hero_bonus"))
end
end
 
function modifier_soul_collector:OnCreepKilled(value)
self:IncStackValue(value)
end
 
function modifier_soul_collector:OnDeath(params)
if not IsServer() then return end

        if params.unit==self:GetCaster() then
        self:IncStackValue(-1*math.floor(self:GetStackCount()/2))
        elseif not params.unit:IsRealHero() then
        	if params.attacker==self:GetCaster() then
        		self:IncStackValue(1)
        	end 
        	return
        end
end

-- Credits: Noya, ZzZombo, DrTeaSpoon