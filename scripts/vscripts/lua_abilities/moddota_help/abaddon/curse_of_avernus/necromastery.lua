ability_abaddon_curse_avernus=class({})
 
LinkLuaModifier("modifier_soul_collector","lua_abilities/moddota_help/abaddon/curse_of_avernus/modifier_soul_collector",LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_frostmourne","lua_abilities/moddota_help/abaddon/curse_of_avernus/modifier_frostmourne",LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_frostmourne_buff","lua_abilities/moddota_help/abaddon/curse_of_avernus/modifier_frostmourne_buff",LUA_MODIFIER_MOTION_NONE)


function ability_abaddon_curse_avernus:GetIntrinsicModifierName()
return "modifier_soul_collector"
end

-- Credits: ZzZombo