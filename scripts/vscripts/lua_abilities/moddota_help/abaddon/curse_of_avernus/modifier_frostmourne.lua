modifier_frostmourne=class({})


function modifier_frostmourne:IsDebuff()
return true
end
 
function modifier_frostmourne:DeclareFunctions()
return {
        MODIFIER_EVENT_ON_ATTACK_LANDED,
        MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
        MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE
}
end
----
function modifier_frostmourne:OnAttackLanded(params)
if not IsServer() then return end
if params.target~=self:GetParent() then return end
params.attacker:AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_frostmourne_buff",{duration=self:GetAbility():GetSpecialValueFor("buff_duration")})
end
----
function modifier_frostmourne:GetModifierMoveSpeedBonus_Percentage()
return -1*self:GetAbility():GetSpecialValueFor("slow_pct")
end
 
function modifier_frostmourne:GetModifierAttackSpeedBonus_Constant()
return -1*self:GetAbility():GetSpecialValueFor("attack_slow_tooltip")
end
 
-- Credits: ZzZombo