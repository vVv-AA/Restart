modifier_frostmourne_buff=class({})


function modifier_frostmourne_buff:IsBuff()
return true
end
 
function modifier_frostmourne_buff:DeclareFunctions()
return {
        MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
        MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
}
end
----
function modifier_frostmourne_buff:GetModifierMoveSpeedBonus_Percentage()
return self:GetAbility():GetSpecialValueFor("move_speed_pct")
end
 
function modifier_frostmourne_buff:GetModifierAttackSpeedBonus_Constant()
return self:GetAbility():GetSpecialValueFor("attack_speed")
end