function AphoticShield( event )
    -- Variables
    -- test block

    if not IsServer() then return end

    local target = event.target
    local caster = event.caster
    local ability = event.ability
    local abilityLevel = ability:GetLevel()

    local damage_dealt = 0
    local damage_pct = ability:GetLevelSpecialValueFor("damage_per_unit_per", (abilityLevel - 1))

    local radius = ability:GetLevelSpecialValueFor("radius", (abilityLevel - 1))

    local units = FindUnitsInRadius( caster:GetTeamNumber(), target:GetAbsOrigin(), nil, radius, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, FIND_ANY_ORDER, DOTA_UNIT_TARGET_FLAG_NONE, false )

    local particle_name = "particles/units/heroes/hero_abaddon/abaddon_death_coil.vpcf"
    -- Play the ability sound
    caster:EmitSound("Hero_Abaddon.DeathCoil.Cast")
    target:EmitSound("Hero_Abaddon.DeathCoil.Target")


    for _,unit in pairs(units) do
        if unit ~= nil and ( not unit:IsMagicImmune() ) and ( not unit:IsInvulnerable() ) then

            local damagetable = {
                    victim = unit,
                    attacker = caster,
                    damage = math.floor( unit:GetMaxHealth() * ( damage_pct / 100 ) ),
                    damage_type = DAMAGE_TYPE_MAGICAL,
                    ability = ability
            }
            if damagetable.victim ~= event.target then
            	if damagetable.victim:GetTeamNumber() == caster:GetTeamNumber() and damagetable.damage>=damagetable.victim:GetHealth()  then
                    damagetable.damage = 0
                    damage_dealt = damage_dealt + (damagetable.victim:GetHealth() - 1)
            		damagetable.victim:SetHealth(1)
                else
                    damage_dealt = damage_dealt + ApplyDamage( damagetable )
            	end
            end
                local info = {
                Target = target,
                Source = damagetable.victim,
                Ability = ability,
                EffectName = particle_name,
                bDodgeable = false,
                bProvidesVision = true,
                iMoveSpeed = 1200,
                iVisionRadius = 0,
                iVisionTeamNumber = caster:GetTeamNumber(),
                iSourceAttachment = DOTA_PROJECTILE_ATTACHMENT_ATTACK_1
                }
                ProjectileManager:CreateTrackingProjectile( info )
        end
    end

    target.max_damage_absorb = damage_dealt
    local shield_size = 75 -- could be adjusted to model scale

    -- Strong Dispel
    local RemovePositiveBuffs = false
    local RemoveDebuffs = true
    local BuffsCreatedThisFrameOnly = false
    local RemoveStuns = true
    local RemoveExceptions = false
    target:Purge( RemovePositiveBuffs, RemoveDebuffs, BuffsCreatedThisFrameOnly, RemoveStuns, RemoveExceptions)

    -- Reset the shield
    target.AphoticShieldRemaining = target.max_damage_absorb

    -- Particle. Need to wait one frame for the older particle to be destroyed
    Timers:CreateTimer(0.01, function()
        target.ShieldParticle = ParticleManager:CreateParticle("particles/units/heroes/hero_abaddon/abaddon_aphotic_shield.vpcf", PATTACH_ABSORIGIN_FOLLOW, target)
        ParticleManager:SetParticleControl(target.ShieldParticle, 1, Vector(shield_size,0,shield_size))
        ParticleManager:SetParticleControl(target.ShieldParticle, 2, Vector(shield_size,0,shield_size))
        ParticleManager:SetParticleControl(target.ShieldParticle, 4, Vector(shield_size,0,shield_size))
        ParticleManager:SetParticleControl(target.ShieldParticle, 5, Vector(shield_size,0,0))

        -- Proper Particle attachment courtesy of BMD. Only PATTACH_POINT_FOLLOW will give the proper shield position
        ParticleManager:SetParticleControlEnt(target.ShieldParticle, 0, target, PATTACH_POINT_FOLLOW, "attach_hitloc", target:GetAbsOrigin(), true)
    end)
end



function AphoticShieldAbsorb( event )
	-- Variables
	local damage = event.DamageTaken
	local unit = event.unit
	local ability = event.ability
	
	-- Track how much damage was already absorbed by the shield
	local shield_remaining = unit.AphoticShieldRemaining
	print("Shield Remaining: "..shield_remaining)
	print("Damage Taken pre Absorb: "..damage)

		if damage > shield_remaining then
			local newHealth = unit.OldHealth - damage + shield_remaining
			print("Old Health: "..unit.OldHealth.." - New Health: "..newHealth.." - Absorbed: "..shield_remaining)
			unit:SetHealth(newHealth)
		else
			local newHealth = unit.OldHealth			
			unit:SetHealth(newHealth)
			print("Old Health: "..unit.OldHealth.." - New Health: "..newHealth.." - Absorbed: "..damage)
		end

		-- Reduce the shield remaining and remove
		unit.AphoticShieldRemaining = unit.AphoticShieldRemaining-damage
		if unit.AphoticShieldRemaining <= 0 then
			unit.AphoticShieldRemaining = nil
			unit:RemoveModifierByName("modifier_aphotic_shield")
			print("--Shield removed--")
		end

		if unit.AphoticShieldRemaining then
			print("Shield Remaining after Absorb: "..unit.AphoticShieldRemaining)
			print("---------------")
		end
end

-- Destroys the particle when the modifier is destroyed. Also plays the sound
function EndShieldParticle( event )
	local target = event.target
	target:EmitSound("Hero_Abaddon.AphoticShield.Destroy")
	ParticleManager:DestroyParticle(target.ShieldParticle,false)
end


-- Keeps track of the targets health
function AphoticShieldHealth( event )
	local target = event.target

	target.OldHealth = target:GetHealth()
end

function AphoticShieldDestoryDamage( event )
    local caster = event.caster
    local target = event.target
    local ability = event.ability
    local abilityLevel = ability:GetLevel()
    print(caster:GetStrength())
    print(target:GetStrength())
    local radius = ability:GetLevelSpecialValueFor("radius", (abilityLevel - 1))

    local units = FindUnitsInRadius( caster:GetTeamNumber(), target:GetAbsOrigin(), nil, radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, FIND_ANY_ORDER, DOTA_UNIT_TARGET_FLAG_NONE, false )

    for _,unit in pairs(units) do
        if unit ~= nil and ( not unit:IsMagicImmune() ) and ( not unit:IsInvulnerable() ) then

            local damagetable = {
                    victim = unit,
                    attacker = caster,
                    damage = target.max_damage_absorb,
                    damage_type = DAMAGE_TYPE_PURE,
                    ability = ability,
            }
            ApplyDamage( damagetable )
        end
    end
    EndShieldParticle( event )
end

--Credits: HewDraw