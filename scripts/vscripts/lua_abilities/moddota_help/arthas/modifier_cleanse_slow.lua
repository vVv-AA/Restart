modifier_cleanse_slow = class({})

function modifier_cleanse_slow:DeclareFunctions()
local funcs = {
        MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
}
return funcs
end
---
function modifier_cleanse_slow:OnCreated( kv )  
    if IsServer() then
   	local ability = self:GetAbility()
    local slow_pct = ability:GetLevelSpecialValueFor("ms_pct", (ability:GetLevel() - 1))
    local duration = ability:GetLevelSpecialValueFor("tooltip_duration", (ability:GetLevel() - 1))
    local tick_number = ability:GetLevelSpecialValueFor("slow_rate", (ability:GetLevel() - 1))

    	self.slow = slow_pct
    	self:StartIntervalThink(duration/tick_number)
    end
end

function modifier_cleanse_slow:OnIntervalThink()
	if IsServer() then
   	local ability = self:GetAbility()
    local duration = ability:GetLevelSpecialValueFor("tooltip_duration", (ability:GetLevel() - 1))
    local tick_number = ability:GetLevelSpecialValueFor("slow_rate", (ability:GetLevel() - 1))

        self.slow = self.slow-(self.slow/tick_number)
	end
end

function modifier_cleanse_slow:OnDestroy()
	if IsServer() then
		if self:GetParent():GetTeamNumber() ~= self:GetCaster():GetTeamNumber() then
            local damageTable = {
	           victim = self:GetParent(),
	           attacker = self:GetCaster(),
	           damage = self:GetAbility():GetAbilityDamage(),
	           damage_type = self:GetAbility():GetAbilityDamageType(),
            }
            ApplyDamage(damageTable)
	   end
    end
end

function modifier_cleanse_slow:IsPurgable()
return false
end

function modifier_cleanse_slow:GetModifierMoveSpeedBonus_Percentage( params )
return self.slow
end