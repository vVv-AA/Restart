LinkLuaModifier( "modifier_cleanse_slow", "lua_abilities/moddota_help/arthas/modifier_cleanse_slow.lua", LUA_MODIFIER_MOTION_NONE )

function CleanseStart( event )
    -- Variables
    -- test block
    if not IsServer() then return end

    local target = event.target
    local caster = event.caster
    local ability = event.ability
    local abilityLevel = ability:GetLevel()

    local duration = ability:GetLevelSpecialValueFor("tooltip_duration", (abilityLevel - 1))
    if target:GetTeamNumber() == caster:GetTeamNumber() then duration = 1 end
    ability:ApplyDataDrivenModifier(caster, target, "effect_mod", {duration = duration}) --[[Returns:void
    No Description Set
    ]]
    -- Strong Dispel
    local RemovePositiveBuffs = false
    local RemoveDebuffs = true
    local BuffsCreatedThisFrameOnly = false
    local RemoveStuns = true
    local RemoveExceptions = false
    target:Purge( RemovePositiveBuffs, RemoveDebuffs, BuffsCreatedThisFrameOnly, RemoveStuns, RemoveExceptions)

    -- Kill the target if it is a summoned unit.
        if target:IsSummoned() then target:Kill(ability, caster)
        else
        target:AddNewModifier(caster, ability, "modifier_cleanse_slow", {duration = duration})
        end
end