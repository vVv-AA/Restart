function CooldownCheck( event )
	local caster = event.caster
	local ability = event.ability
	local sacred_duty_level = ability:GetLevel()
	-- Current ability handles, always on the first 2 slots
	local ability1 = caster:GetAbilityByIndex(0)
	local ability2 = caster:GetAbilityByIndex(1)
	local ability3 = caster:GetAbilityByIndex(2)
	local ability4 = caster:GetAbilityByIndex(3)
	local penalty_cooldown = ability:GetLevelSpecialValueFor("penalty_cooldown", ability:GetLevel() - 1)
	-- Cooldowns
	local cooldown1 = ability1:GetCooldown(ability1:GetLevel())
	local cooldown2 = ability2:GetCooldown(ability2:GetLevel())
	local cooldown3 = ability3:GetLevelSpecialValueFor( "cooldown", ( ability:GetLevel() - 1 ) )
	local cooldown4 = ability4:GetCooldown(ability4:GetLevel())


if event.event_ability:GetName() == "item_refresher" then
	ability3:EndCooldown()
	caster.sacred_duty_charges = 1
	if caster:HasModifier("modifier_spell1_datadriven") == true then
	caster:RemoveModifierByName("modifier_spell1_datadriven")
	else
	ability1:EndCooldown()
	end
	if caster:HasModifier("modifier_spell2_datadriven") == true then
	caster:RemoveModifierByName("modifier_spell2_datadriven")
	else
	ability2:EndCooldown()
	end
	if caster:HasModifier("modifier_spell4_datadriven") == true then
	caster:RemoveModifierByName("modifier_spell4_datadriven")
	else
	ability4:EndCooldown()
	end
	return
end

if caster.sacred_duty_charges == 1 then
	if event.event_ability == ability1 then
			if caster:HasModifier("modifier_spell1_datadriven") ~= true then
				ability:ApplyDataDrivenModifier(caster, caster, "modifier_spell1_datadriven", { duration = cooldown1 })
				ability1:EndCooldown()
			else
				ability1:EndCooldown()
				ability1:StartCooldown(cooldown1*(1+penalty_cooldown) - caster:FindModifierByName("modifier_spell1_datadriven"):GetRemainingTime())
				caster:RemoveModifierByName("modifier_spell1_datadriven")
				
				if caster:HasModifier("modifier_spell2_datadriven") then
					ability2:StartCooldown(caster:FindModifierByName("modifier_spell2_datadriven"):GetRemainingTime())
					caster:RemoveModifierByName("modifier_spell2_datadriven")
				end
				
				ability3:EndCooldown()
				ability3:StartCooldown(cooldown3)
				caster.sacred_duty_charges = 0
				
				if caster:HasModifier("modifier_spell4_datadriven") then
					ability4:StartCooldown(caster:FindModifierByName("modifier_spell4_datadriven"):GetRemainingTime())
					caster:RemoveModifierByName("modifier_spell4_datadriven")
				end
			end
	end

	if event.event_ability == ability2 then
			if caster:HasModifier("modifier_spell2_datadriven") ~= true then
				ability:ApplyDataDrivenModifier(caster, caster, "modifier_spell2_datadriven", { duration = cooldown2 })
				ability2:EndCooldown()
			else
				if caster:HasModifier("modifier_spell1_datadriven") then
					ability1:StartCooldown(caster:FindModifierByName("modifier_spell1_datadriven"):GetRemainingTime())
					caster:RemoveModifierByName("modifier_spell1_datadriven")
				end

				ability2:EndCooldown()
				ability2:StartCooldown(cooldown2*(1+penalty_cooldown) - caster:FindModifierByName("modifier_spell2_datadriven"):GetRemainingTime())
				caster:RemoveModifierByName("modifier_spell2_datadriven")
							
				ability3:EndCooldown()
				ability3:StartCooldown(cooldown3)
				caster.sacred_duty_charges = 0

				if caster:HasModifier("modifier_spell4_datadriven") then
					ability4:StartCooldown(caster:FindModifierByName("modifier_spell4_datadriven"):GetRemainingTime())
					caster:RemoveModifierByName("modifier_spell4_datadriven")
				end
			end
	end


	if event.event_ability == ability4 then
			if caster:HasModifier("modifier_spell4_datadriven") ~= true then
				ability:ApplyDataDrivenModifier(caster, caster, "modifier_spell4_datadriven", { duration = cooldown4 })
				ability4:EndCooldown()
			else
				if caster:HasModifier("modifier_spell1_datadriven") then
					ability1:StartCooldown(caster:FindModifierByName("modifier_spell1_datadriven"):GetRemainingTime())
					caster:RemoveModifierByName("modifier_spell1_datadriven")
				end

				if caster:HasModifier("modifier_spell2_datadriven") then
					ability2:StartCooldown(caster:FindModifierByName("modifier_spell2_datadriven"):GetRemainingTime())
					caster:RemoveModifierByName("modifier_spell2_datadriven")
				end
							
				ability3:EndCooldown()
				ability3:StartCooldown(cooldown3)
				caster.sacred_duty_charges = 0

				ability4:EndCooldown()
				ability4:StartCooldown(cooldown4*(1+penalty_cooldown) - caster:FindModifierByName("modifier_spell4_datadriven"):GetRemainingTime())
				caster:RemoveModifierByName("modifier_spell4_datadriven")
			end
	end
end

end

function SacredDutyCooldown ( keys )

	if keys.ability:GetLevel() ~= 1 then return end
	-- Variables
	local caster = keys.caster
	local ability = keys.ability
	local modifierName = "modifier_sacred_duty_counter_datadriven"
	local charge_replenish_time = ability:GetLevelSpecialValueFor( "cooldown", ( ability:GetLevel() - 1 ) )
	
	-- Initialize stack
	caster:SetModifierStackCount( modifierName, caster, 0 )
	caster.sacred_duty_charges = 1
	caster.start_charge = false
	caster.sacred_duty_cooldown = 0.0

	ability:ApplyDataDrivenModifier( caster, caster, modifierName, {} )
	caster:SetModifierStackCount( modifierName, caster, 1 )
	
	-- create timer to restore stack
	Timers:CreateTimer( function()
			-- Restore charge
			if caster.start_charge and caster.sacred_duty_charges < 1 then
				-- Calculate stacks
				local next_charge = caster.sacred_duty_charges + 1
				caster:RemoveModifierByName( modifierName )
				if next_charge ~= 1 then
					ability:ApplyDataDrivenModifier( caster, caster, modifierName, { duration = charge_replenish_time } )
					sacred_duty_start_cooldown( caster, charge_replenish_time )
				else
					ability:ApplyDataDrivenModifier( caster, caster, modifierName, {} )
					caster.start_charge = false
				end
				caster:SetModifierStackCount( modifierName, caster, next_charge )
				
				-- Update stack
				caster.sacred_duty_charges = next_charge
			end
			
			-- Check if max is reached then check every 0.5 seconds if the charge is used
			if caster.sacred_duty_charges ~= 1 then
				caster.start_charge = true
				return charge_replenish_time
			else
				return 0.5
			end
		end
	)
end

function sacred_duty_start_cooldown( caster, charge_replenish_time )
	caster.sacred_duty_cooldown = charge_replenish_time
	Timers:CreateTimer( function()
			local current_cooldown = caster.sacred_duty_cooldown - 0.1
			if current_cooldown > 0.1 then
				caster.sacred_duty_cooldown = current_cooldown
				return 0.1
			else
				return nil
			end
		end
	)
end

--Credits: Perry, 