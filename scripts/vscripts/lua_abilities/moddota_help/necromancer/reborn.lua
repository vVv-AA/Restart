function PreCast( keys )
	local marker = keys.target
	local target = marker:GetOwner()
	local caster = keys.caster
	if marker:GetUnitName() == "tombstone_marker" then
		local target = marker:GetOwner()
		local ability = keys.ability
		if target:GetTimeUntilRespawn() <= ability:GetChannelTime() then
			caster:Stop()
		end
	else
		caster:Stop()
	end
end

function IncreaseRespawnTime( keys )
	local target = keys.target:GetOwner()
	local ability = keys.ability
	target:SetTimeUntilRespawn( target:GetTimeUntilRespawn() + ability:GetSpecialValueFor("respawn_time_penalty") )
end

function RespawnHero( keys )
	local marker = keys.target
	local target = marker:GetOwner()
	local respawnPos = marker:GetAbsOrigin()
	local ability = keys.ability
	target:SetRespawnPosition(respawnPos)
	target:SetTimeUntilRespawn(0.0)
		marker:Kill( ability, marker )
end

function FireSound( keys )
	local target = keys.target
	local sound = keys.sound
	EmitSoundOn( sound, target ) 
end

function StopSoundEffect( keys )
	local target = keys.target
	local sound = keys.sound
	StopSoundEvent( sound, target )
end