call_of_the_dead_debuff = class({}) 

function call_of_the_dead_debuff:IsPurgable()
return false
end

function call_of_the_dead_debuff:OnCreated()
        self:SetStackCount(0)
end

function call_of_the_dead_debuff:IsDebuff()
return true
end
 
function call_of_the_dead_debuff:DeclareFunctions()
return {
        MODIFIER_EVENT_ON_ATTACKED,
        MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
        MODIFIER_PROPERTY_MOVESPEED_BONUS_CONSTANT
}
end
----
function call_of_the_dead_debuff:OnAttacked(params)
        if not IsServer() then return end
        self:SetStackCount(math.min(self:GetStackCount() + 1, 5))
end

function call_of_the_dead_debuff:GetModifierMoveSpeedBonus_Constant()
return -1*self:GetAbility():GetSpecialValueFor("move_slow_pct")*self:GetStackCount()
end
 
function call_of_the_dead_debuff:GetModifierAttackSpeedBonus_Constant()
return -1*self:GetAbility():GetSpecialValueFor("attack_slow")*self:GetStackCount()
end

function call_of_the_dead_debuff:OnDestroy( params )
        self:SetStackCount(0)
end