function DarkRitual( event )
	local caster = event.caster
	local point = event.target_points[1]
	local ability = event.ability
			print("Yes")
local ShieldParticle
	-- Mana to give	
	local rate = ability:GetSpecialValueFor( "health_conversion_fraction" )
	local health_gain
	local radius = ability:GetSpecialValueFor( "radius" )

    local units = FindUnitsInRadius( caster:GetTeamNumber(), point, nil, 125, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_BASIC, FIND_ANY_ORDER, DOTA_UNIT_TARGET_FLAG_NOT_ANCIENTS + DOTA_UNIT_TARGET_FLAG_NOT_CREEP_HERO, false )
	
	for _,unit in pairs(units) do
		if true==true then
    ShieldParticle= ParticleManager:CreateParticle("particles/units/heroes/hero_lich/lich_dark_ritual.vpcf", PATTACH_CUSTOMORIGIN, hero)
    ParticleManager:SetParticleControl(ShieldParticle, 0, unit:GetAbsOrigin())
    ParticleManager:SetParticleControl(ShieldParticle, 1, unit:GetAbsOrigin())


			health_gain = unit:GetHealth() * rate
			caster:SetHealth( caster:GetHealth() + health_gain )

			-- Purple particle with eye
			local particleName = "particles/msg_fx/msg_xp.vpcf"
			local particle = ParticleManager:CreateParticle(particleName, PATTACH_OVERHEAD_FOLLOW, unit)

			local digits = 0
			if health_gain ~= nil then
			digits = #tostring(health_gain)
			end

			ParticleManager:SetParticleControl(particle, 1, Vector(9, health_gain, 6))
			ParticleManager:SetParticleControl(particle, 2, Vector(1, digits+1, 0))
			ParticleManager:SetParticleControl(particle, 3, Vector(170, 0, 250))

			-- Kill the target, ForceKill doesn't grant xp
			unit:ForceKill(true)
		end
	end
end
