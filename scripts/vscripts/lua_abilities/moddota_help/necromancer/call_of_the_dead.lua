function IncrementStack( keys )
    -- Variables
    local caster = keys.caster
    local target = keys.target
    local ability = keys.ability
    local modifierName = "modifier_call_of_the_dead_target_datadriven"
    local modifierOwnerHas = ""
    local modifierDeBuffName = "modifier_call_of_the_dead_stack_counter"
    local modifierCount = target:GetModifierCount()


    -- Necessary value from KV
    local duration = ability:GetSpecialValueFor( "duration" )
    local current_stack = 0
        for i = 0, modifierCount do
            modifierOwnerHas = target:GetModifierNameByIndex(i)

            if modifierOwnerHas == modifierDeBuffName then
                current_stack = current_stack + 1
            end
        end

        -- Remove all the old buff modifiers
        for i = 0, current_stack do
            target:RemoveModifierByName(modifierDeBuffName)
        end
        
        target:SetModifierStackCount( modifierName, ability, current_stack + 1 )

        for i = 1, current_stack + 1 do
            ability:ApplyDataDrivenModifier(caster, target, modifierDeBuffName, {})
        end
end