function RaiseDeadCheck( keys )

	local caster = keys.caster
	local target = keys.target
	local ability = keys.ability
	local cost
	ability.unit_count = ability.unit_count or 0

	if target:IsAncient() then
		cost = ability.unit_count + 4
	elseif target:IsSummoned() then
		cost = ability.unit_count + 3
	elseif target:GetAbilityByIndex(0) ~= nil then
		cost = ability.unit_count + 2
	else
		cost = ability.unit_count + 1
	end

	if cost < 8 and target:IsAlive() ~= true and target:GetUnitName() ~= "npc_dota_roshan" then
		local unit = CreateUnitByName(target:GetUnitName(), target:GetAbsOrigin(), true, caster, caster, caster:GetTeamNumber())
		caster:ReduceMana(caster:GetMaxMana()*5/100)
		unit:SetMaxHealth(target:GetMaxHealth()) -- For dominated/persuaded creeps
		ability:ApplyDataDrivenModifier(caster, unit, "raised_modifier", {}) --keep track of unit_count

		ability.unit_count = cost

		Timers:CreateTimer(FrameTime(), function()
			FindClearSpaceForUnit( unit, unit:GetAbsOrigin(), false )
			return nil
		end) --FindClearSpaceForUnit after 1 frame so creeps don't get stuck. Does not work in line 19 if multiple creeps are killed.

		unit:SetControllableByPlayer(caster:GetPlayerID() , true)
		unit:SetOwner(caster)
	end
end

function UnitDied( keys )
	if keys.target:IsAncient() then
		keys.ability.unit_count = keys.ability.unit_count - 4
	elseif keys.target:IsSummoned() then
		keys.ability.unit_count = keys.ability.unit_count - 3
	elseif keys.target:GetAbilityByIndex(0) ~= nil then
		keys.ability.unit_count = keys.ability.unit_count - 2
	else
		keys.ability.unit_count = keys.ability.unit_count - 1
	end
end

function CheckCalloftheDead( keys )
    local owner = keys.target
    if owner:HasModifier("modifier_call_of_the_dead_ally_datadriven") == false then
        owner:SetHealth(math.max(owner:GetHealth() - owner:GetMaxHealth()*0.01*0.02, 1))
        else
        owner:Heal(owner:GetMaxHealth()*0.01*0.02, nil)
    end
end