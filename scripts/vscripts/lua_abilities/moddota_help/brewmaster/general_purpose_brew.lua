function GiveStack0( keys )
	local modifierName = keys.modifierName
	local caster = keys.caster
	local ability = keys.ability
	
	if ability:GetLevel() - 1 ~= 0 then return
	else
		ability:ApplyDataDrivenModifier(caster, caster, modifierName, {}) --[[Returns:void
		No Description Set
		]]
		if caster:HasModifier(modifierName) == true then
			local modifier = caster:FindModifierByNameAndCaster(modifierName, caster)
			caster:SetModifierStackCount(modifierName, caster, 0)
		end
	end

	if caster:HasModifier(modifierName) then
		print("#################")
	 print(modifierName) 
	 print("&&&&&&&&&&&&&&&&&") --[[Returns:bool
	Sees if this unit has a given modifier
	]]end
end

function HasCharge( keys )
	local caster = keys.caster
	local ability = keys.ability
	local charge_modifier = keys.modifierName
	print("Knock")
	if caster:HasModifier(charge_modifier) then
		print("Yes1")
		local charges = caster:GetModifierStackCount(charge_modifier, caster)
				print(charges)
		if charges <= 0 then
			caster:Stop()
			return
		end
	end
end

function ReduceCharge( keys )
	local ability = keys.ability
	local caster = keys.caster
	local modifierName = keys.modifierName
	local charges = caster:GetModifierStackCount(modifierName, caster)
	print("Yes2")
	caster:SetModifierStackCount(modifierName, caster, charges - 1)
end