modifier_brew_counter_datadriven=class({})


function modifier_brew_counter_datadriven:IsBuff()
return true
end

function modifier_brew_counter_datadriven:IsPassive()
return true
end

function modifier_brew_counter_datadriven:OnCreated()
        if not IsServer() return end
        local caster = self:GetParent()
        caster.can_brew_stacks = 6
end