modifier_unstable_concoction_brewing=class({})


function modifier_unstable_concoction_brewing:IsBuff()
return true
end
 
function modifier_unstable_concoction_brewing:OnCreated( kv )
        if not IsServer() return end
        local caster = kv.caster
        local ability = kv.self
        local brew_time = kv.brew_time
        self:StartIntervalThink( 0.5 )
end

function modifier_unstable_concoction_brewing:OnIntervalThink()
        if not IsServer() return end
        local caster = self:GetParent()
        local ability = self:GetAbility()
        self:UpdateTimerParticle()
end

function modifier_unstable_concoction_brewing:OnDestroy()
        if not IsServer() return end
        local caster = self:GetParent()
        local ability = self:GetAbility()
        self:CheckBrewComplete()
end



function modifier_unstable_concoction_brewing:UpdateTimerParticle()
        local caster = self:GetParent()
        local ability = self:GetAbility()
        local brew_time = ability:GetSpecialValueFor( "brew_time" )

        -- Show the particle to all allies
        local allHeroes = HeroList:GetAllHeroes()
        local particleName = "particles/units/heroes/hero_alchemist/alchemist_unstable_concoction_timer.vpcf"
        local preSymbol = 0 -- Empty
        local digits = 2 -- "5.0" takes 2 digits
        local number = GameRules:GetGameTime() - ability.brew_start - brew_time - 0.1 --the minus .1 is needed because think interval comes a frame earlier

        -- Get the integer. Add a bit because the think interval isn't a perfect 0.5 timer
        local integer = math.floor(math.abs(number))

        -- Round the decimal number to .0 or .5
        local decimal = math.abs(number) % 1

        if decimal < 0.5 then 
                decimal = 1 -- ".0"
        else 
                decimal = 8 -- ".5"
        end

        print(integer,decimal)

        for k, v in pairs( allHeroes ) do
                if v:GetPlayerID() and v:GetTeam() == caster:GetTeam() then
                        -- Don't display the 0.0 message
                        if integer == 0 and decimal == 1 then
                                
                        else
                                local particle = ParticleManager:CreateParticleForPlayer( particleName, PATTACH_OVERHEAD_FOLLOW, caster, PlayerResource:GetPlayer( v:GetPlayerID() ) )
                                
                                ParticleManager:SetParticleControl( particle, 0, caster:GetAbsOrigin() )
                                ParticleManager:SetParticleControl( particle, 1, Vector( preSymbol, integer, decimal) )
                                ParticleManager:SetParticleControl( particle, 2, Vector( digits, 0, 0) )
                        end
                end
        end
end

function modifier_unstable_concoction_brewing:CheckBrewComplete()
        local caster = self:GetParent()
        local ability = self:GetAbility()
        local brew_time = ability:GetSpecialValueFor("brew_time")
        ability.time_charged = GameRules:GetGameTime() - ability.brew_start
        if ability.time_charged >= brew_time then
                if ability.brew_modifier == "modifier_heal_spell_datadriven" then
                        if caster:HasModifier("heal_spell_stack") then
                                caster:SetModifierStackCount("heal_spell_stack", caster, caster:GetModifierStackCount("heal_spell_stack", caster) + 1 )
                        end

                elseif ability.brew_modifier == "modifier_rage_spell_datadriven" then
                        if caster:HasModifier("rage_spell_stack") then
                                caster:SetModifierStackCount("rage_spell_stack", caster, caster:GetModifierStackCount("rage_spell_stack", caster) + 1 )
                        end

                elseif ability.brew_modifier == "modifier_jump_spell_datadriven" then print("Jump")
                        if caster:HasModifier("jump_spell_stack") then print ("Stack")
                                caster:SetModifierStackCount("jump_spell_stack", caster, caster:GetModifierStackCount("jump_spell_stack", caster) + 1 )
                        end

                elseif ability.brew_modifier == "modifier_freeze_spell_datadriven" then
                        if caster:HasModifier("freeze_spell_stack") then
                                caster:SetModifierStackCount("freeze_spell_stack", caster, caster:GetModifierStackCount("freeze_spell_stack", caster) + 1 )
                        end
                else
                        print("Unhandled Event")
                end
        else
                print("Brewing Canceled")
        end

        caster:SwapAbilities(ability:GetAbilityName(), "brew_end_datadriven" , true, false)
end