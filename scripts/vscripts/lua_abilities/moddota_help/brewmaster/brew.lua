function SwitchModifiers( keys )
	local caster = keys.caster
	local modifierOwnerHas
    local modifierCount = caster:GetModifierCount()
    local modifierList = {}
    local ability = keys.ability

    modifierList[1] = "modifier_heal_spell_datadriven"
    modifierList[2] = "modifier_rage_spell_datadriven"
    modifierList[3] = "modifier_jump_spell_datadriven"
    modifierList[4] = "modifier_freeze_spell_datadriven"
    local mod_int = 0
    local foundModifierName = nil
    local i = 0
    local j = 0
    for i = 0, modifierCount do
        modifierOwnerHas = caster:GetModifierNameByIndex(i)
        for j = 1, 4 do 
	        if modifierOwnerHas == modifierList[j] then
	            mod_int = j
	            foundModifierName = modifierOwnerHas
	            break
	        end
	    end
	    if foundModifierName ~= nil then break end
    end

    if foundModifierName ~= nil then
    	caster:RemoveModifierByNameAndCaster(foundModifierName, caster)
    	print("####")
    	print(modifierList[(mod_int%4) + 1])
    	print("$$$$")
    	ability:ApplyDataDrivenModifier(caster, caster, modifierList[(mod_int%4) + 1], {})
    else
    	ability:ApplyDataDrivenModifier(caster, caster, modifierList[1], {})
    end
    ability.brew_modifier = modifierList[(mod_int%4) + 1]
    print(ability.brew_modifier)
end