brew_lua_based = class ({})
--Link Modifiers so they can be called here.
LinkLuaModifier( "modifier_brew_counter_datadriven", "lua_abilities/moddota_help/brewmaster/modifier_brew_counter_datadriven.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_unstable_concoction_brewing", "lua_abilities/moddota_help/brewmaster/modifier_unstable_concoction_brewing.lua", LUA_MODIFIER_MOTION_NONE )

function brew_lua_based:OnAbilityPhaseStart()
        if not IsServer() return end
        local caster = self:GetCaster()
        if caster.can_brew_stacks <= 0 then
                        print("Fail")
                return
        else
                local max_spell_slot = self:GetSpecialValueFor("max_spell_slot")
                if caster:HasModifier("heal_spell_stack") then
                        max_spell_slot = max_spell_slot - caster:GetModifierStackCount("heal_spell_stack", caster)
                end
                if caster:HasModifier("freeze_spell_stack") then
                        max_spell_slot = max_spell_slot - caster:GetModifierStackCount("freeze_spell_stack", caster)
                end
                if caster:HasModifier("rage_spell_stack") then
                        max_spell_slot = max_spell_slot - caster:GetModifierStackCount("rage_spell_stack", caster)
                end
                if caster:HasModifier("jump_spell_stack") then
                        max_spell_slot = max_spell_slot - caster:GetModifierStackCount("jump_spell_stack", caster)
                end
                if max_spell_slot <=0 then
                         print("Fail")
                         return
                 end
        end
end
--Executed OnSpellStart, after cast phase.
function brew_lua_based:OnSpellStart()
        if not IsServer() return end
        local  caster = self:GetCaster()
        local brew_time = self:GetSpecialValueFor("brew_time")

        caster:AddNewModifier(caster, self, "modifier_unstable_concoction_brewing", {Duration = brew_time})
        local brew_modifier = caster:GetAbilityByIndex(0).brew_modifier
        local modifierName = "modifier_brew_counter_datadriven"
        self.brew_modifier = brew_modifier
        self.brew_start = GameRules:GetGameTime()
        caster:SwapAbilities(self:GetAbilityName(), "brew_end_datadriven" , false, true)

        -- Play the sound, which will be stopped when the sub ability fires
        caster:EmitSound("Hero_Alchemist.UnstableConcoction.Fuse")
        local maximum_charges = self:GetSpecialValueFor( "max_spell_slot" )
        local charge_replenish_time = self:GetSpecialValueFor( "charge_restore_time" )

        caster:RemoveModifierByName( modifierName )
        caster:AddNewModifier(caster, self, modifierName, {Duration = charge_replenish_time})
        start_cooldown( caster, charge_replenish_time )
        if caster.can_brew_stack == maximum_charges then
                caster:RemoveModifierByName( modifierName )
                caster:AddNewModifier(caster, self, modifierName, {Duration = charge_replenish_time})
                start_cooldown( caster, charge_replenish_time )
        end
        caster.can_brew_stack = caster.can_brew_stack - 1
        caster:SetModifierStackCount( modifierName, self, caster.can_brew_stack )

        if caster.can_brew_stack == 0 then
                -- Start Cooldown from caster.brew_cooldown
                self:StartCooldown( caster.brew_cooldown )
        else
                self:EndCooldown()
        end
end

function brew_lua_based:OnUpgrade()
        if not IsServer() return end
        start_charge(self:GetOwner(), self)
end
function start_cooldown( caster, charge_replenish_time )
        caster.brew_cooldown = charge_replenish_time
        Timers:CreateTimer( function()
                        local current_cooldown = caster.brew_cooldown - 0.1
                        if current_cooldown > 0.1 then
                                caster.brew_cooldown = current_cooldown
                                return 0.1
                        else
                                return nil
                        end
                end
        )
end


function start_charge( caster, ability )
        -- Initial variables to keep track of different max charge requirements
        caster.can_brew_stack_max = ability:GetSpecialValueFor( "max_spell_slot" )

        -- Only start charging at level 1
        if ability:GetLevel() ~= 1 then return end

        -- Variables
        local modifierName = "modifier_brew_counter_datadriven"
        local charge_replenish_time = ability:GetSpecialValueFor( "charge_restore_time" )
        
        -- Initialize stack
        caster:SetModifierStackCount( modifierName, ability, 0 )
        caster.can_brew_stack = caster.can_brew_stack_max
        caster.start_charge = false
        caster.brew_cooldown = 0.0
        
        caster:AddNewModifier(caster, ability, modifierName, {})
        caster:SetModifierStackCount( modifierName, ability, caster.can_brew_stack_max )
        
        -- create timer to restore stack
        Timers:CreateTimer( function()
                        -- Restore charge
                        if caster.start_charge and caster.can_brew_stack < caster.can_brew_stack_max then
                                -- Calculate stacks
                                local next_charge = caster.can_brew_stack + 1
                                caster:RemoveModifierByName( modifierName )
                                if next_charge ~= caster.can_brew_stack_max then
                                        caster:AddNewModifier(caster, ability, modifierName, {Duration = charge_replenish_time})
                                        start_cooldown( caster, charge_replenish_time )
                                else
                                        caster:AddNewModifier(caster, ability, modifierName, {})
                                        caster.start_charge = false
                                end
                                caster:SetModifierStackCount( modifierName, ability, next_charge )
                                
                                -- Update stack
                                caster.can_brew_stack = next_charge
                        end
                        
                        -- Check if max is reached then check every 0.5 seconds if the charge is used
                        if caster.can_brew_stack ~= caster.can_brew_stack_max then
                                caster.start_charge = true
                                return charge_replenish_time
                        else
                                return 0.5
                        end
                end
        )
end