function CheckCharge( keys )
	
	local caster = keys.caster
	caster.can_cast = true
	if caster.can_brew_stack <= 0 then
		caster.can_cast = false
		print("blocked")
	else
		local max = 6
		if caster:HasModifier("heal_spell_stack") then
			max = max - caster:GetModifierStackCount("heal_spell_stack", caster)
		end
		if caster:HasModifier("freeze_spell_stack") then
			max = max - caster:GetModifierStackCount("freeze_spell_stack", caster)
		end
		if caster:HasModifier("rage_spell_stack") then
			max = max - caster:GetModifierStackCount("rage_spell_stack", caster)
		end
		if caster:HasModifier("jump_spell_stack") then
			max = max - caster:GetModifierStackCount("jump_spell_stack", caster)
		end
		print(max)
		if max <=0 then caster.can_cast = false end
	end
end

function StartBrewing( keys )
	local caster = keys.caster
    local ability = keys.ability
	if caster.can_brew_stack > 0 and caster.can_cast == true then
		ability:ApplyDataDrivenModifier(caster, caster, "modifier_unstable_concoction_brewing", {})
	    local brew_modifier = caster:GetAbilityByIndex(0).brew_modifier
	    local modifierName = "modifier_brew_counter_datadriven"
	    ability.brew_modifier = brew_modifier
		ability.brew_start = GameRules:GetGameTime()
	    caster:SwapAbilities(ability:GetAbilityName(), "brew_end_datadriven" , false, true)
		-- Play the sound, which will be stopped when the sub ability fires
		caster:EmitSound("Hero_Alchemist.UnstableConcoction.Fuse")
		local maximum_charges = ability:GetSpecialValueFor( "max_spell_slot" )
		local charge_replenish_time = ability:GetSpecialValueFor( "charge_restore_time" )

		caster:RemoveModifierByName( modifierName )
		ability:ApplyDataDrivenModifier( caster, caster, modifierName, { Duration = charge_replenish_time } )
		start_cooldown( caster, charge_replenish_time + 0.2 )
		if caster.can_brew_stack == maximum_charges then
			caster:RemoveModifierByName( modifierName )
			ability:ApplyDataDrivenModifier( caster, caster, modifierName, { Duration = charge_replenish_time } )
			start_cooldown( caster, charge_replenish_time )
		end
		caster.can_brew_stack = caster.can_brew_stack - 1
		caster:SetModifierStackCount( modifierName, ability, caster.can_brew_stack )
		
		if caster.can_brew_stack == 0 then
			-- Start Cooldown from caster.brew_cooldown
			ability:StartCooldown( caster.brew_cooldown )
		else
			ability:EndCooldown()
		end
	end
end

function start_cooldown( caster, charge_replenish_time )
	caster.brew_cooldown = charge_replenish_time
	Timers:CreateTimer( function()
			local current_cooldown = caster.brew_cooldown - 0.1
			if current_cooldown > 0.1 then
				caster.brew_cooldown = current_cooldown
				return 0.1
			else
				return nil
			end
		end
	)
end


function start_charge( keys )
	-- Initial variables to keep track of different max charge requirements
	local caster = keys.caster
	local ability = keys.ability
	caster.can_brew_stack_max = ability:GetSpecialValueFor( "max_spell_slot" )

	-- Only start charging at level 1
	if keys.ability:GetLevel() ~= 1 then return end

	-- Variables
	local modifierName = keys.modifier_name --"modifier_brew_counter_datadriven"
	local charge_replenish_time = ability:GetSpecialValueFor( "charge_restore_time" )
	
	-- Initialize stack
	caster:SetModifierStackCount( modifierName, ability, 0 )
	caster.can_brew_stack = caster.can_brew_stack_max
	caster.start_charge = false
	caster.brew_cooldown = 0.0
	
	ability:ApplyDataDrivenModifier( caster, caster, modifierName, {} )
	caster:SetModifierStackCount( modifierName, ability, caster.can_brew_stack_max )
	
	-- create timer to restore stack
	Timers:CreateTimer( function()
			-- Restore charge
			if caster.start_charge and caster.can_brew_stack < caster.can_brew_stack_max then
				-- Calculate stacks
				local next_charge = caster.can_brew_stack + 1
				caster:RemoveModifierByName( modifierName )
				if next_charge ~= caster.can_brew_stack_max then
					ability:ApplyDataDrivenModifier( caster, caster, modifierName, { Duration = charge_replenish_time } )
					start_cooldown( caster, charge_replenish_time )
				else
					ability:ApplyDataDrivenModifier( caster, caster, modifierName, {} )
					caster.start_charge = false
				end
				caster:SetModifierStackCount( modifierName, ability, next_charge )
				
				-- Update stack
				caster.can_brew_stack = next_charge
			end
			
			-- Check if max is reached then check every 0.5 seconds if the charge is used
			if caster.can_brew_stack ~= caster.can_brew_stack_max then
				caster.start_charge = true
				return charge_replenish_time
			else
				return 0.5
			end
		end
	)
end


function UpdateTimerParticle( event )
if IsServer() then
	local caster = event.caster
	local ability = event.ability
	local brew_time = ability:GetSpecialValueFor( "brew_time" )

	-- Show the particle to all allies
	local allHeroes = HeroList:GetAllHeroes()
	local particleName = "particles/units/heroes/hero_alchemist/alchemist_unstable_concoction_timer.vpcf"
	local preSymbol = 0 -- Empty
	local digits = 2 -- "5.0" takes 2 digits
	local number = GameRules:GetGameTime() - ability.brew_start - brew_time - 0.1 --the minus .1 is needed because think interval comes a frame earlier

	-- Get the integer. Add a bit because the think interval isn't a perfect 0.5 timer
	local integer = math.floor(math.abs(number))

	-- Round the decimal number to .0 or .5
	local decimal = math.abs(number) % 1

	if decimal < 0.5 then 
		decimal = 1 -- ".0"
	else 
		decimal = 8 -- ".5"
	end

	print(integer,decimal)

	for k, v in pairs( allHeroes ) do
		if v:GetPlayerID() and v:GetTeam() == caster:GetTeam() then
			-- Don't display the 0.0 message
			if integer == 0 and decimal == 1 then
				
			else
				local particle = ParticleManager:CreateParticleForPlayer( particleName, PATTACH_OVERHEAD_FOLLOW, caster, PlayerResource:GetPlayer( v:GetPlayerID() ) )
				
				ParticleManager:SetParticleControl( particle, 0, caster:GetAbsOrigin() )
				ParticleManager:SetParticleControl( particle, 1, Vector( preSymbol, integer, decimal) )
				ParticleManager:SetParticleControl( particle, 2, Vector( digits, 0, 0) )
			end
		end
	end
end
end

function CheckBrewComplete( keys )
	local ability = keys.ability
	local caster = keys.caster
	local brew_time = ability:GetSpecialValueFor("brew_time")
	ability.time_charged = GameRules:GetGameTime() - ability.brew_start
	if ability.time_charged >= brew_time then
		if ability.brew_modifier == "modifier_heal_spell_datadriven" then
			if caster:HasModifier("heal_spell_stack") then
				caster:SetModifierStackCount("heal_spell_stack", caster, caster:GetModifierStackCount("heal_spell_stack", caster) + 1 )
			end

		elseif ability.brew_modifier == "modifier_rage_spell_datadriven" then
			if caster:HasModifier("rage_spell_stack") then
				caster:SetModifierStackCount("rage_spell_stack", caster, caster:GetModifierStackCount("rage_spell_stack", caster) + 1 )
			end

		elseif ability.brew_modifier == "modifier_jump_spell_datadriven" then print("Jump")
			if caster:HasModifier("jump_spell_stack") then print ("Stack")
				caster:SetModifierStackCount("jump_spell_stack", caster, caster:GetModifierStackCount("jump_spell_stack", caster) + 1 )
			end

		elseif ability.brew_modifier == "modifier_freeze_spell_datadriven" then
			if caster:HasModifier("freeze_spell_stack") then
				caster:SetModifierStackCount("freeze_spell_stack", caster, caster:GetModifierStackCount("freeze_spell_stack", caster) + 1 )
			end
		else
			print("Unhandled Event")
		end
	else
		print("Brewing Canceled")
	end

	caster:SwapAbilities(ability:GetAbilityName(), "brew_end_datadriven" , true, false)
end