function shadowinDay( event )
	if GameRules:IsDaytime() == false then return end
	local caster = event.caster
	local ability = event.ability

	if caster:IsAttacking() == false and ability:IsCooldownReady() and caster:HasModifier("modifier_invis") == false then
		ability:ApplyDataDrivenModifier(caster, caster, "modifier_invis", nil)
	elseif caster:IsAttacking() then
		ability:StartCooldown(ability:GetLevelSpecialValueFor("fade_delay_day", ability:GetLevel() - 1))
	end
end

function shadowinNight( event )
	if GameRules:IsDaytime() == true then return end
	local caster = event.caster
	local ability = event.ability

	if caster:IsAttacking() == false and ability:IsCooldownReady() and caster:HasModifier("modifier_invis") == false then
		ability:ApplyDataDrivenModifier(caster, caster, "modifier_invis", nil)
	elseif caster:IsAttacking() then
		ability:StartCooldown(ability:GetLevelSpecialValueFor("fade_delay_night", ability:GetLevel() - 1))
	end
end