function checkAutoCast (keys)
	local caster = keys.caster
	local ability = keys.ability

	print(ability:GetAutoCastState())
	if ability:GetAutoCastState() == true then
		if caster:HasModifier("bonus_range_mod") == false then
			ability:ApplyDataDrivenModifier(caster, caster, "bonus_range_mod", {})
			caster:SetAcquisitionRange(caster:GetAcquisitionRange() + ability:GetSpecialValueFor("bonus_range"))
		end
	else
		if caster:HasModifier("bonus_range_mod") == true then
			caster:RemoveModifierByNameAndCaster("bonus_range_mod", caster)
			caster:SetAcquisitionRange(caster:GetAcquisitionRange() - ability:GetSpecialValueFor("bonus_range"))
		end
	end
end