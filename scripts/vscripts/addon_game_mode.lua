-- This is the entry-point to your game mode and should be used primarily to precache models/particles/sounds/etc

require('internal/util')
require('restart')


 if restart == nil then
     restart = class({})
 end



function Precache( context )
--[[
  This function is used to precache resources/units/items/abilities that will be needed
  for sure in your game and that will not be precached by hero selection.  When a hero
  is selected from the hero selection screen, the game will precache that hero's assets,
  any equipped cosmetics, and perform the data-driven precaching defined in that hero's
  precache{} block, as well as the precache{} block for any equipped abilities.

  See restart:PostLoadPrecache() in gamemode.lua for more information
  ]]

  DebugPrint("[RESTART] Performing pre-load precache")

  -- Particles can be precached individually or by folder
  -- It it likely that precaching a single particle system will precache all of its children, but this may not be guaranteed
  PrecacheResource("particle", "particles/econ/generic/generic_aoe_explosion_sphere_1/generic_aoe_explosion_sphere_1.vpcf", context)
  PrecacheResource("particle_folder", "particles/test_particle", context)

  -- Models can also be precached by folder or individually
  -- PrecacheModel should generally used over PrecacheResource for individual models
  PrecacheResource("model_folder", "particles/heroes/antimage", context)
  PrecacheResource("model", "particles/heroes/viper/viper.vmdl", context)
  PrecacheModel("models/heroes/viper/viper.vmdl", context)
  PrecacheUnitByNameSync("fog_unit", context) 
  PrecacheUnitByNameSync("tombstone_marker", context) 
  -- Sounds can precached here like anything else
  PrecacheResource("soundfile", "soundevents/game_sounds_heroes/game_sounds_gyrocopter.vsndevts", context)

  -- Entire items can be precached by name
  -- Abilities can also be precached in this way despite the name
  PrecacheItemByNameSync("example_ability", context)
  PrecacheItemByNameSync("item_example_item", context)

  -- Entire heroes (sound effects/voice/models/particles) can be precached with PrecacheUnitByNameSync
  -- Custom units from npc_units_custom.txt can also have all of their abilities and precache{} blocks precached in this way
  PrecacheUnitByNameSync("npc_dota_hero_ancient_apparition", context)
  PrecacheUnitByNameSync("npc_dota_hero_enigma", context)
end


function restart:Initrestart()
    GameRules:GetGameModeEntity():SetExecuteOrderFilter( Dynamic_Wrap(restart, "OrderFilter"), self )
    ListenToGameEvent( "dota_player_pick_hero", Dynamic_Wrap( restart, "OnPlayerPicked" ), self )
    ListenToGameEvent("entity_killed", Dynamic_Wrap(restart, "OnEntityKilled"), self)
    ListenToGameEvent( "npc_spawned", Dynamic_Wrap( restart, "OnNPCSpawned" ), self )
end

function restart:OrderFilter( order )
local self1 = EntIndexToHScript( order["units"]["0"] ) 
  if self1:HasModifier("post_poss_stun_enemy_particles") == true then
  local o_type = order.order_type
  if o_type == 12 or o_type == 13 or o_type ==  14 or o_type ==  16 or o_type ==  17 or o_type ==  18 or o_type ==  25 or o_type ==  24 then
      return false end
    end
  return true
  -- body
end

function restart:OnPlayerPicked( keys )
    local spawnedUnitIndex = EntIndexToHScript(keys.heroindex)
    if spawnedUnitIndex:GetClassname() == "npc_dota_hero_antimage" then
    spawnedUnitIndex:GetAbilityByIndex(3):SetLevel(1)
    elseif spawnedUnitIndex:GetClassname() == "npc_dota_hero_alchemist" then
    spawnedUnitIndex:GetAbilityByIndex(0):SetLevel(1)
    spawnedUnitIndex:GetAbilityByIndex(1):SetLevel(1)
    spawnedUnitIndex:GetAbilityByIndex(6):SetLevel(1)
    end
end

function restart:OnEntityKilled( keys )
  local killedUnit = EntIndexToHScript( keys.entindex_killed )
  -- The Killing entity
  local killerEntity = nil

  if keys.entindex_attacker ~= nil then
    killerEntity = EntIndexToHScript( keys.entindex_attacker )
  end

  if killedUnit:IsRealHero() then
    local unit = CreateUnitByName("tombstone_marker", killedUnit:GetAbsOrigin(), true, killedUnit, killedUnit, killedUnit:GetTeamNumber())
      killedUnit.dummy = unit
  end
end

function restart:OnNPCSpawned( keys )
    local spawnedUnit = EntIndexToHScript( keys.entindex )
    if spawnedUnit:IsRealHero() then
      if IsValidEntity(spawnedUnit.dummy) == true and spawnedUnit.dummy:IsAlive() == true then
        spawnedUnit.dummy:ForceKill(true)
      end
    end
end

-- Create the game mode when we activate

function Activate()
  GameRules.restart = restart()
  GameRules.restart:Initrestart()
end
